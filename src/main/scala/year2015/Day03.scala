package year2015

import common.{Cell, ResourceLoader}
import common.Sequences._

import scala.collection.MapView

object Day03 extends App with ResourceLoader {

  def housePositions(directions: Seq[Char]): Seq[Cell] = {
    directions.scanLeft(Cell(0, 0)) { (cell, direction) =>
      direction match {
        case '<' => cell.copy(column = cell.column - 1)
        case '>' => cell.copy(column = cell.column + 1)
        case '^' => cell.copy(row = cell.row - 1)
        case 'v' => cell.copy(row = cell.row + 1)
      }
    }
  }

  val data = load("day03-input.txt").mkString("")

  def countByHouse(santas: Int): Int = {
    val journeys: Map[Int, Seq[Char]] = data.toCharArray.toSeq.split(santas)
    journeys.values.toSet.flatMap(housePositions).size
  }

  println(countByHouse(1))
  println(countByHouse(2))
}
