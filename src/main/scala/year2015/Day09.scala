package year2015

import common.{AocParserBuilder, ResourceLoader}

object Day09 extends App with ResourceLoader {

  case class Route(start: String, finish: String, length: Int)
  object RouteParserBuilder extends AocParserBuilder[Route] {

    def word: Parser[String] = """[A-Za-z]+""".r ^^ { _.toString }
    def number: Parser[Int] = """\d+""".r ^^ { _.toInt }

    override def build: Parser[Route] =
      (word <~ "to") ~ (word <~ "=") ~ number ^^ {
        case start ~ finish ~ length =>
          Route(start, finish, length)
      }
  }

  val testData = """
                   |Dublin -> London -> Belfast = 982
                   |London -> Dublin -> Belfast = 605
                   |London -> Belfast -> Dublin = 659
                   |Dublin -> Belfast -> London = 659
                   |Belfast -> Dublin -> London = 605
                   |Belfast -> London -> Dublin = 982""".rows

  val data = load("day09-input.txt")

  val routeParser = RouteParserBuilder()
  val routes = data.flatMap(routeParser.maybeParse)
  val allPlaces = routes.flatMap(route => Seq(route.start, route.finish)).toSet

  val permutations = allPlaces.toSeq.permutations
  val trips = permutations.map { destinations =>
    val hops = destinations.sliding(2, 1).toSeq
    val distances = for {
      hop <- hops
      start = hop.head
      finish = hop(1)
      route <-
        routes
          .find(route =>
            (route.start == start && route.finish == finish) || (route.start == finish && route.finish == start)
          )
          .toSeq
    } yield {
      route.length
    }
    val distance = distances.sum
    (destinations, distance)
  }.toSeq

  println(trips.minBy(_._2)) // 207
  println(trips.maxBy(_._2)) // 804
}
