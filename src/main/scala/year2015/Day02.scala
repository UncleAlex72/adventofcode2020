package year2015

import common.ResourceLoader
import common.Sequences._

import scala.util.Try

object Day02 extends App with ResourceLoader {

  case class Dimension(length: Int, width: Int, height: Int) {

    def wrappingRequired: Int = {
      val areas = Seq(length * width, width * height, height * length)
      2 * areas.sum + areas.min
    }

    def ribbonRequired: Long = {
      val sizes = Seq(length, width, height).sorted.reverse
      val smallerSizes = sizes.tail
      2 * smallerSizes.sum + sizes.multiply
    }
  }

  object Dimension {
    def apply(str: String): Option[Dimension] = {
      val regex = "([0-9]+)x([0-9]+)x([0-9]+)".r
      str match {
        case regex(lengthStr, widthStr, heightStr) =>
          def parse(str: String): Try[Int] = Try(Integer.parseInt(str))
          val tryDimension = for {
            length <- parse(lengthStr)
            width <- parse(widthStr)
            height <- parse(heightStr)
          } yield {
            Dimension(length, width, height)
          }
          tryDimension.toOption
      }
    }
  }

  val data = load("day02-input.txt")

  val dimensions: Seq[Dimension] = data.flatMap(Dimension(_))
  println(dimensions.map(_.wrappingRequired).sum)
  println(dimensions.map(_.ribbonRequired).sum)
}
