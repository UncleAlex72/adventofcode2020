package year2015

import common.{AocParserBuilder, ResourceLoader}
import common.Sequences._

object Day15 extends App with ResourceLoader {

  val testData =
    """
      |Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
      |Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3""".rows

  case class Ingredient(
      name: String,
      capacity: Int = 0,
      durability: Int = 0,
      flavour: Int = 0,
      texture: Int = 0,
      calories: Int = 0
  )
  object IngredientParserBuilder extends AocParserBuilder[Ingredient] {
    def word: Parser[String] = """[A-Za-z]+""".r ^^ { _.toString }
    def number: Parser[Int] = """-?\d+""".r ^^ { _.toInt }

    def name: Parser[Ingredient] =
      word <~ ":" ^^ { name =>
        Ingredient(name)
      }

    def property(
        name: String,
        fn: (Ingredient, Int) => Ingredient
    ): Parser[Ingredient => Ingredient] = {
      name ~> number ^^ { i =>
        fn(_, i)
      }
    }

    def properties: Parser[Ingredient => Ingredient] = {
      val builders: Seq[(String, (Ingredient, Int) => Ingredient)] = Seq(
        "capacity" -> ((ingredient: Ingredient, capacity: Int) =>
          ingredient.copy(capacity = capacity)
        ),
        "durability" -> ((ingredient: Ingredient, durability: Int) =>
          ingredient.copy(durability = durability)
        ),
        "flavor" -> ((ingredient: Ingredient, flavour: Int) =>
          ingredient.copy(flavour = flavour)
        ),
        "texture" -> ((ingredient: Ingredient, texture: Int) =>
          ingredient.copy(texture = texture)
        ),
        "calories" -> ((ingredient: Ingredient, calories: Int) =>
          ingredient.copy(calories = calories)
        )
      )
      val empty: Seq[Parser[Ingredient => Ingredient]] = Seq.empty
      val parsers = builders.foldLeft(empty) {
        case (parsers, (name, builder)) =>
          val parser = name ~> number ^^ { value => ingredient: Ingredient =>
            builder(ingredient, value)
          }
          val maybePrefixedParser =
            if (parsers.isEmpty) parser else "," ~> parser
          parsers :+ maybePrefixedParser
      }
      parsers.tail.foldLeft(parsers.head)(_ | _)
    }

    override def build: Parser[Ingredient] = {
      name ~ properties.* ^^ {
        case ingredient ~ properties =>
          properties.foldLeft(ingredient)((i, f) => f(i))
      }

    }
  }

  def calculate(
      maybeRequiredCalories: Option[Int]
  )(amountsByIngredient: Map[Ingredient, Int]): Long = {
    val calorieCount: Option[Long] = maybeRequiredCalories.flatMap {
      requiredCalories =>
        val calorieCount = amountsByIngredient.map {
          case (ingredient, amount) =>
            ingredient.calories * amount
        }.sum
        if (calorieCount == requiredCalories) {
          None
        } else {
          Some(0)
        }
    }
    calorieCount.getOrElse {
      val extractors: Seq[Ingredient => Int] =
        Seq(_.capacity, _.durability, _.flavour, _.texture)
      val amounts: Seq[Int] = extractors.map { extractor =>
        val amountsForProperty = amountsByIngredient.map {
          case (ingredient, amount) =>
            extractor(ingredient) * amount
        }
        amountsForProperty.sum
      }
      amounts.map(amount => Math.max(0, amount)).multiply
    }
  }

  def optimise(
      ingredients: Seq[Ingredient],
      amount: Int,
      calories: Option[Int]
  ): Map[Ingredient, Int] = {
    val sumToAmount: Seq[Seq[Int]] =
      (1 to amount).combinations(ingredients.size).filter(_.sum == amount).toSeq

    val amountsByIngredients: Seq[Map[Ingredient, Int]] = for {
      values <- sumToAmount
      ingredient <- ingredients.permutations
    } yield {
      ingredient.zip(values).toMap
    }
    amountsByIngredients.maxBy(calculate(calories))
  }

  val data = load("day15-input.txt")
  val ingredientParser = IngredientParserBuilder()
  val ingredients = data.flatMap(ingredientParser.maybeParse)

  private val partOneOptimum: Map[Ingredient, Int] =
    optimise(ingredients, 100, None)
  println(s"${calculate(None)(partOneOptimum)}: $partOneOptimum") //18965440

  private val partTwoOptimum: Map[Ingredient, Int] =
    optimise(ingredients, 100, Some(500))
  println(
    s"${calculate(Some(500))(partTwoOptimum)}: $partTwoOptimum"
  ) //18965440

}
