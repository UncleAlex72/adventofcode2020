package year2015

import common.ResourceLoader

import java.io.{FileWriter, PrintWriter}

object Day05 extends App with ResourceLoader {

  val data = load("day05-input.txt")

  trait Block {

    val predicates: LazyList[String => Boolean]

    def execute(data: Seq[String]): Unit = {
      val niceStings = data.count(str => predicates.forall(pred => pred(str)))
      println(niceStings)
    }
  }

  object PartOne extends Block {

    object ContainsAtLeastThreeVowels extends (String => Boolean) {

      def letterCount(str: String, letters: Char*): LazyList[Map[Char, Int]] = {
        val empty: Map[Char, Int] = Map.empty
        str.toCharArray.to(LazyList).scanLeft(empty) {
          (countsByLetter, letter) =>
            if (letters.contains(letter)) {
              val currentCountForLetter = countsByLetter.getOrElse(letter, 0)
              countsByLetter + (letter -> (currentCountForLetter + 1))
            } else {
              countsByLetter
            }
        }
      }

      def apply(str: String): Boolean = {
        letterCount(str, 'a', 'e', 'i', 'o', 'u').exists(_.values.sum >= 3)
      }
    }

    object ContainsRepeatedLetters extends (String => Boolean) {

      case class State(
          previousCharacter: Option[Char] = None,
          matches: Boolean = false
      )

      def repeatedLetters(str: String): LazyList[State] = {
        str.toCharArray.to(LazyList).scanLeft(State()) {
          (state, currentCharacter) =>
            val matches = state.previousCharacter.contains(currentCharacter)
            State(previousCharacter = Some(currentCharacter), matches = matches)
        }
      }

      def apply(str: String): Boolean = {
        repeatedLetters(str).map(_.matches).find(identity).getOrElse(false)
      }
    }

    object DoesNotContainStrings extends (String => Boolean) {

      def apply(str: String): Boolean = {
        LazyList("ab", "cd", "pq", "xy").forall(subStr => !str.contains(subStr))
      }
    }

    val predicates: LazyList[String => Boolean] = LazyList(
      DoesNotContainStrings,
      ContainsRepeatedLetters,
      ContainsAtLeastThreeVowels
    )
  }

  PartOne.execute(data)

  object PartTwo extends Block {

    object ContainsRepeatedPair extends (String => Boolean) {

      def apply(str: String): Boolean = {
        val indicies = LazyList.iterate(0)(_ + 1).takeWhile(_ < str.length - 1)
        indicies.exists { idx =>
          val firstBit = str.substring(0, idx)
          val thisBit = str.substring(idx, idx + 2)
          val nextBit = str.substring(idx + 2)
          firstBit.contains(thisBit) || nextBit.contains(thisBit)
        }
      }
    }

    object ContainsSameLetterWithOneLetterBetween extends (String => Boolean) {

      def apply(str: String): Boolean = {
        val indicies = LazyList.iterate(0)(_ + 1).takeWhile(_ < str.length - 2)
        indicies.exists { idx =>
          val thisChar = str.charAt(idx)
          val lastChar = str.charAt(idx + 2)
          thisChar == lastChar
        }
      }
    }

    val predicates: LazyList[String => Boolean] = LazyList(
      ContainsRepeatedPair,
      ContainsSameLetterWithOneLetterBetween
    )
  }

  PartTwo.execute(data)
}
