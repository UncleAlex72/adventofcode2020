package year2015

import common.ResourceLoader
import io.circe.Json._
import io.circe._
import io.circe.parser._

object Day12 extends App with ResourceLoader {

  val source = loadSingle("day12-input.txt")

  val json: Json = parse(source).toOption.get

  def count(json: Json, ignoreRed: Boolean): Long = {
    def _count(json: Json): Long = {
      json.asNumber
        .flatMap(_.toLong)
        .orElse(json.asArray.map(_.map(_count).sum))
        .orElse(json.asObject.map { obj =>
          val values = obj.values
          if (ignoreRed && values.exists(_.asString.contains("red"))) {
            0
          } else {
            values.map(_count).sum
          }
        })
        .getOrElse(0)
    }
    _count(json)
  }

  println(count(json, ignoreRed = false)) // 191164
  println(count(json, ignoreRed = true)) // 87842
}
