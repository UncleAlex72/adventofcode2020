package year2015

import common.ResourceLoader

object Day01 extends App with ResourceLoader {

  val data = load("day01-input.txt").mkString("")

  val result = data.toCharArray.toSeq.scanLeft(0) { (value, character) =>
    character match {
      case '(' => value + 1
      case ')' => value - 1
      case _   => value
    }
  }

  println(result.last)
  println(result.zipWithIndex.find {
    case (value, _) => value == -1
  })
}
