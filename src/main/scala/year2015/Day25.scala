package year2015

import common.{Cell, ResourceLoader}

object Day25 extends App with ResourceLoader {

  def diagonal(startRow: Int): LazyList[Cell] = {
    val neverEndingDiagonal =
      LazyList.iterate(Cell(row = startRow, column = 1)) { cell =>
        Cell(column = cell.column + 1, row = cell.row - 1)
      }
    neverEndingDiagonal.takeWhile(cell => cell.row > 0)
  }

  val allCells = LazyList.iterate(1)(_ + 1).flatMap(diagonal)
  val codes = LazyList.iterate(20151125L)(_ * 252533 % 33554393)
  val cellsAndCodes = allCells.zip(codes)

  def codeAt(idx: Int): Long = {
    var (result, counter) = (20151125L, idx)
    while (counter != 0) {
      result = result * 252533 % 33554393
      counter -= 1
    }
    result
  }

  val cellIdx = allCells.zipWithIndex
    .find {
      case (cell, _) =>
        cell.row == 3010 && cell.column == 3019
    }
    .map(_._2)
    .head

  println(codeAt(cellIdx))
}
