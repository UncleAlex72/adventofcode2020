package year2015

import common.{AocParserBuilder, ResourceLoader}

import scala.collection.immutable.SortedMap
import scala.collection.mutable
import scala.util.parsing.combinator._

object Day08 extends App with ResourceLoader {

  case class CodeString(codeData: String, stringData: String) {
    def +(other: CodeString): CodeString =
      CodeString(codeData + other.codeData, stringData + other.stringData)
  }
  object CodeStringParserBuilder extends AocParserBuilder[CodeString] {

    def escapedQuote: Parser[CodeString] =
      literal("""\"""") ^^ { str =>
        CodeString(str, """"""")
      }
    def escapedBackslash: Parser[CodeString] =
      literal("""\\""") ^^ { str =>
        CodeString(str, """\""")
      }
    def escapedHexCode: Parser[CodeString] =
      (literal("""\x""") ~ """[a-f0-9]{2}""".r) ^^ {
        case str ~ num =>
          CodeString(str + num, Integer.parseInt(num, 16).toChar.toString)
      }
    def anyOtherChar: Parser[CodeString] =
      """[^"]""".r ^^ { str =>
        CodeString(str, str)
      }

    def delim: Parser[CodeString] =
      literal(""""""") ^^ { str =>
        CodeString(str, "")
      }

    def anyChar: Parser[CodeString] =
      escapedHexCode | escapedBackslash | escapedQuote | anyOtherChar

    def build: Parser[CodeString] =
      (delim ~ anyChar.* ~ delim) ^^ {
        case l ~ c ~ r =>
          (c :+ r).foldLeft(l)(_ + _)
      }
  }

  val testData =
    """
      |""
      |"abc"
      |"aaa\"aaa"
      |"\x27"
      |""".stripMargin.split("\n").filterNot(_.isEmpty)

  val data = load("day08-input.txt")

  val codeStringParser = CodeStringParserBuilder()
  val codeStrings = data.flatMap(codeStringParser.maybeParse)

  val differences = codeStrings.map {
    case CodeString(codeData, stringData) => codeData.length - stringData.length
  }
  println(differences.sum) //1350

  val encodedDifference = data.map { line =>
    val newLine = {
      s""""${line.replace("""\""", """\\""").replace(""""""", """\"""")}""""
    }
    newLine.length - line.length
  }.sum

  println(encodedDifference)
}
