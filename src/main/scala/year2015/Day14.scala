package year2015

import common.{AocParserBuilder, ResourceLoader}

object Day14 extends App with ResourceLoader {

  case class Reindeer(
      name: String,
      speed: Int,
      flyingSeconds: Int,
      restingSeconds: Int
  ) {

    val totalSecondsPerInterval: Int = flyingSeconds + restingSeconds
    val distancePerInterval: Int = speed * flyingSeconds

    def distanceAt(seconds: Int): Int = {
      val totalIntervals = seconds / totalSecondsPerInterval
      val secondsInThisInterval = seconds % totalSecondsPerInterval
      totalIntervals * distancePerInterval + Math.min(
        flyingSeconds,
        secondsInThisInterval
      ) * speed
    }
  }

  object ReindeerParserBuilder extends AocParserBuilder[Reindeer] {
    def name: Parser[String] = """[A-Z][a-z]+""".r ^^ { _.toString }
    def number: Parser[Int] = """\d+""".r ^^ { _.toInt }

    override def build: Parser[Reindeer] = {
      (name <~ "can fly") ~ number ~ ("km/s for" ~> number <~ "seconds, but then must rest for") ~ (number <~ "seconds.") ^^ {
        case name ~ speed ~ flyingSeconds ~ restingSeconds =>
          Reindeer(name, speed, flyingSeconds, restingSeconds)
      }
    }
  }

  val testData =
    """
      |Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
      |Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
      |""".rows

  val reindeerParser = ReindeerParserBuilder()
  val reindeers = load("day14-input.txt").flatMap(reindeerParser.maybeParse)

  def speedWinnersAt(seconds: Int): (Seq[Reindeer], Int) = {
    val standings = reindeers.map { reindeer =>
      reindeer -> reindeer.distanceAt(seconds)
    }
    val bestDistance = standings.map(_._2).max
    val winners = standings.filter(_._2 == bestDistance).map(_._1)
    (winners, seconds)
  }
  val speedWinners = speedWinnersAt(2503)
  println(
    s"${speedWinners._1.map(_.name).mkString(", ")}: ${speedWinners._2}"
  ) // Cupid: 2696

  def pointsWinnerAt(seconds: Int): (Seq[Reindeer], Int) = {
    val initialPoints: Map[Reindeer, Int] = reindeers.map(_ -> 0).toMap
    val finalPoints = (1 to seconds).foldLeft(initialPoints) {
      (currentPoints, currentSeconds) =>
        val winners = speedWinnersAt(currentSeconds)._1
        winners.foldLeft(currentPoints) { (newPoints, winner) =>
          newPoints + (winner -> (newPoints.getOrElse(winner, 0) + 1))
        }
    }
    val bestPoints = finalPoints.values.max
    val winners = finalPoints.filter(_._2 == bestPoints).keys.toSeq
    (winners, bestPoints)
  }

  val pointsWinner = pointsWinnerAt(2503)
  println(
    s"${pointsWinner._1.map(_.name).mkString(", ")}: ${pointsWinner._2}"
  ) // Rudolf: 1084
}
