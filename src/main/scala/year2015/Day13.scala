package year2015

import common.{AocParserBuilder, ResourceLoader}

object Day13 extends App with ResourceLoader {

  case class Happiness(first: String, second: String, offset: Int)

  object HappinessParserBuilder extends AocParserBuilder[Happiness] {
    def name: Parser[String] = """[A-Z][a-z]+""".r ^^ { _.toString }
    def number: Parser[Int] = """\d+""".r ^^ { _.toInt }
    def gainOrLose: Parser[Int] =
      ("gain" ^^ { _ =>
        1
      } | "lose" ^^ { _ =>
        -1
      })
    def happiness: Parser[Int] =
      gainOrLose ~ number ^^ {
        case mult ~ number => mult * number
      }
    override def build: Parser[Happiness] = {
      (name <~ "would") ~ happiness ~ ("happiness units by sitting partOne to" ~> name <~ ".") ^^ {
        case first ~ value ~ second =>
          Happiness(first, second, value)
      }
    }
  }

  val testData =
    """
      |Alice would gain 54 happiness units by sitting partOne to Bob.
      |Alice would lose 79 happiness units by sitting partOne to Carol.
      |Alice would lose 2 happiness units by sitting partOne to David.
      |Bob would gain 83 happiness units by sitting partOne to Alice.
      |Bob would lose 7 happiness units by sitting partOne to Carol.
      |Bob would lose 63 happiness units by sitting partOne to David.
      |Carol would lose 62 happiness units by sitting partOne to Alice.
      |Carol would gain 60 happiness units by sitting partOne to Bob.
      |Carol would gain 55 happiness units by sitting partOne to David.
      |David would gain 46 happiness units by sitting partOne to Alice.
      |David would lose 7 happiness units by sitting partOne to Bob.
      |David would gain 41 happiness units by sitting partOne to Carol.""".rows

  val happinessParser = HappinessParserBuilder()
  val happinesses =
    load("day13-input.txt").flatMap(happinessParser.maybeParse)

  def optimumHappiness(happinesses: Seq[Happiness]): Int = {
    val names: Seq[String] = happinesses
      .flatMap(happinesses => Seq(happinesses.first, happinesses.second))
      .distinct

    val offsets = names.permutations.map { people =>
      val pairs = (people :+ people.head).sliding(2, 1)
      val allPairs = pairs.flatMap {
        case Seq(a, b) => Seq(Seq(a, b), Seq(b, a))
      }
      allPairs.flatMap {
        case Seq(a, b) =>
          happinesses.find(h => h.first == a && h.second == b).map(_.offset)
      }.sum
    }
    offsets.max
  }
  println(optimumHappiness(happinesses)) // 789

  val names: Seq[String] = happinesses
    .flatMap(happinesses => Seq(happinesses.first, happinesses.second))
    .distinct

  val newHappinesses = names.flatMap(name =>
    Seq(Happiness(name, "Me", 0), Happiness("Me", name, 0))
  )

  println(optimumHappiness(happinesses ++ newHappinesses)) // 668

}
