package year2015

import java.security.MessageDigest

object Day04 extends App {

  val SECRET_KEY = "yzbqklnj"

  case class Hash(number: Int) {
    val hash: String = MessageDigest
      .getInstance("MD5")
      .digest((SECRET_KEY + number).getBytes)
      .map("%02X".format(_))
      .mkString

  }
  val numbers: LazyList[Int] = LazyList.iterate(1)(_ + 1)

  println(numbers.map(Hash).find(_.hash.startsWith("00000")))
  println(numbers.map(Hash).find(_.hash.startsWith("000000")))
}
