package year2015

import common.{Cell, ResourceLoader}

object Day06 extends App with ResourceLoader {

  val initialGrid: Map[Cell, Int] = {
    val range = 0 to 999
    val gridSeq = for {
      column <- range
      row <- range
    } yield {
      Cell(column = column, row = row)
    }
    gridSeq.map(cell => cell -> 0).toMap
  }

  def rectangle(topLeft: Cell, bottomRight: Cell): Seq[Cell] = {
    for {
      column <- topLeft.column to bottomRight.column
      row <- topLeft.row to bottomRight.row
    } yield {
      Cell(column = column, row = row)
    }
  }

  case class Modifiers(
      onTurnOn: Int => Int,
      onTurnOff: Int => Int,
      onToggle: Int => Int
  )
  val data = load("day06-input.txt")

  def finalCount(
      onTurnOn: Int => Int,
      onTurnOff: Int => Int,
      onToggle: Int => Int
  ): Int = {
    val finalGrid = data.foldLeft(initialGrid) {
      case (grid, instruction) =>
        val regex =
          "(turn on|turn off|toggle) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)".r
        instruction match {
          case regex(action, left, top, right, bottom) =>
            val cells = rectangle(
              Cell(left.toInt, top.toInt),
              Cell(right.toInt, bottom.toInt)
            )
            val modifier: Int => Int = action match {
              case "turn on"  => onTurnOn
              case "turn off" => onTurnOff
              case "toggle"   => onToggle
              case _          => identity
            }
            cells.foldLeft(grid) { (currentGrid, cell) =>
              val currentStatus = currentGrid.getOrElse(cell, 0)
              val newStatus = modifier(currentStatus)
              currentGrid + (cell -> newStatus)
            }
          case _ => grid
        }
    }
    finalGrid.values.sum
  }

  println(
    finalCount(onTurnOn = _ => 1, onTurnOff = _ => 0, onToggle = x => 1 - x)
  )
  println(
    finalCount(
      onTurnOn = _ + 1,
      onTurnOff = x => Math.max(0, x - 1),
      onToggle = _ + 2
    )
  )
}
