package year2015

import common.{AocParserBuilder, ResourceLoader}

import scala.collection.immutable.SortedMap
import scala.collection.mutable

object Day07 extends App with ResourceLoader {

  sealed trait RegisterOrConstant {
    def registers: Seq[Register]
  }
  case class Register(name: String) extends RegisterOrConstant {
    override def registers: Seq[Register] = Seq(this)
  }
  case class Constant(value: Int) extends RegisterOrConstant {
    override def registers: Seq[Register] = Seq.empty
  }

  sealed trait Expression {
    def registers: Seq[Register]
  }
  case class Value(registerOrConstant: RegisterOrConstant) extends Expression {
    override def registers: Seq[Register] = registerOrConstant.registers
  }
  case class And(left: RegisterOrConstant, right: RegisterOrConstant)
      extends Expression {
    override def registers: Seq[Register] = left.registers ++ right.registers
  }
  case class Or(left: RegisterOrConstant, right: RegisterOrConstant)
      extends Expression {
    override def registers: Seq[Register] = left.registers ++ right.registers
  }
  case class Not(operand: RegisterOrConstant) extends Expression {
    override def registers: Seq[Register] = operand.registers
  }
  case class LeftShift(left: RegisterOrConstant, right: RegisterOrConstant)
      extends Expression {
    override def registers: Seq[Register] = left.registers
  }
  case class RightShift(left: RegisterOrConstant, right: RegisterOrConstant)
      extends Expression {
    override def registers: Seq[Register] = left.registers
  }

  case class Instruction(expression: Expression, target: String)

  object InstructionParserBuilder extends AocParserBuilder[Instruction] {

    def register: Parser[String] = """[a-z]+""".r ^^ { _.toString }
    def number: Parser[Int] = """(\d+)""".r ^^ { _.toInt }
    def registerOrNumber: Parser[RegisterOrConstant] = {
      register.map(Register) | number.map(Constant)
    }
    def binary: Parser[Expression] = {
      def build(
          token: String,
          builder: (RegisterOrConstant, RegisterOrConstant) => Expression
      ): Parser[Expression] = {
        (registerOrNumber ~ literal(token) ~ registerOrNumber).map {
          case left ~ _ ~ right =>
            builder(left, right)
        }
      }
      build("AND", And) | build("OR", Or) | build("LSHIFT", LeftShift) | build(
        "RSHIFT",
        RightShift
      )
    }

    def unary: Parser[Expression] = {
      (literal("NOT") ~ registerOrNumber).map {
        case _ ~ operand => Not(operand)
      }
    }

    def assign: Parser[Expression] = {
      registerOrNumber.map(Value)
    }

    def expression: Parser[Expression] = {
      binary | unary | assign
    }

    override def build: Parser[Instruction] = {
      (expression ~ literal("->") ~ register).map {
        case expression ~ _ ~ register =>
          Instruction(expression, register)
      }
    }

  }

  val empty: Map[String, Int] = SortedMap.empty
  val testData = Seq(
    "123 -> x",
    "456 -> y",
    "x AND y -> d",
    "x OR y -> e",
    "x LSHIFT 2 -> f",
    "y RSHIFT 2 -> g",
    "NOT x -> h",
    "NOT y -> i"
  )

  val data = load("day07-input.txt")
  val instructionParser = InstructionParserBuilder()
  val instructions: Seq[Either[String, Instruction]] =
    data.map(line => instructionParser.parse(line))

  val invalidInstructionIndexes = instructions.zipWithIndex.zip(data).flatMap {
    case ((e, idx), str) =>
      e match {
        case Left(msg) => Some(s"${idx + 1} : $str : $msg")
        case _         => None
      }
  }
  def findValue(instructions: Seq[Instruction], register: String): Int = {
    // Mutability is hidden from the outside so no-one need know.
    val cache: mutable.Map[String, Int] = mutable.HashMap.empty
    def _findValue(register: String): Int = {
      def valueOf(registerOrConstant: RegisterOrConstant): Int = {
        registerOrConstant match {
          case Register(name) =>
            cache.get(name) match {
              case Some(value) => value
              case None =>
                val value = _findValue(name)
                cache += (name -> value)
                value
            }
          case Constant(value) => value
        }
      }
      instructions.find(_.target == register) match {
        case Some(instruction) =>
          instruction.expression match {
            case Value(value)            => valueOf(value)
            case And(left, right)        => valueOf(left) & valueOf(right)
            case Or(left, right)         => valueOf(left) | valueOf(right)
            case Not(operand)            => ~valueOf(operand)
            case LeftShift(left, right)  => valueOf(left) << valueOf(right)
            case RightShift(left, right) => valueOf(left) >> valueOf(right)
          }
        case None => 0
      }
    }
    _findValue(register)
  }
  if (invalidInstructionIndexes.nonEmpty) {
    println(s"Errors at lines: ${invalidInstructionIndexes.mkString(", ")}")
  } else {
    val validInstructions = instructions.flatMap(_.toOption)
    // Part 1
    val partOne = findValue(validInstructions, "a")
    println(s"Part One: $partOne") // 46065
    val updatedInstructions = validInstructions.filterNot(
      _.target == "b"
    ) :+ Instruction(Value(Constant(partOne)), "b")
    val partTwo = findValue(updatedInstructions, "a")
    println(s"Part Two: $partTwo") // 14134
  }

}
