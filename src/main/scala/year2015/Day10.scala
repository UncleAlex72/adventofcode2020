package year2015

import cats.Eval

object Day10 extends App {

  def splitString(str: String): Seq[String] = {
    def _splitString(chars: List[Char]): Eval[List[String]] = {
      chars match {
        case Nil => Eval.now(List.empty)
        case x :: _ =>
          val (these, those) = chars.span(c => c == x)
          Eval.defer(_splitString(those)).map(ts => these.mkString :: ts)
      }
    }
    _splitString(str.toCharArray.toList).value
  }

  def seeSay(str: String): String = {
    splitString(str).map(str => s"${str.length}${str.charAt(0)}").mkString
  }

  def conwaySequence(steps: Int): String = {
    (1 to steps).foldLeft("3113322113") { (str, _) => seeSay(str) }
  }

  println(s"${conwaySequence(40).length}")
  println(s"${conwaySequence(50).length}")
}
