package year2015

object Day11 extends App {

  case class Password(chars: List[Char]) {

    val validCharacters: List[Char] =
      ('a' to 'z').filterNot(ch => "iol".toCharArray.contains(ch)).toList

    def next: Password = {
      chars match {
        case Nil => Password(List.empty)
        case ch :: chs =>
          val maybeNextChar = {
            LazyList
              .iterate(ch)(c => (c + 1).toChar)
              .drop(1)
              .takeWhile(_ <= 'z')
              .find(validCharacters.contains)
          }
          maybeNextChar match {
            case Some(nextChar) =>
              Password(nextChar :: chs)
            case None =>
              Password(validCharacters.head :: Password(chs).next.chars)
          }
      }
    }

    def containsTriple: Boolean = {
      val triplets =
        ('c' to 'z').map(ch => ch :: (ch - 1).toChar :: (ch - 2).toChar :: Nil)
      triplets.exists(triplet => chars.containsSlice(triplet))
    }

    def containsPairs: Boolean = {
      val nonOverlappingPairLocations = chars
        .sliding(2, 1)
        .zipWithIndex
        .filter {
          case (a :: b :: Nil, _) =>
            a == b
        }
        .map(_._2)
        .toSeq
        .combinations(2)
        .count(ss => Math.abs(ss.head - ss(1)) != 1)
      nonOverlappingPairLocations >= 2
    }

    def isValid: Boolean = containsTriple && containsPairs

    override def toString: String = chars.reverse.mkString
  }

  object Password {

    def apply(password: String): Password = {
      Password(password.toCharArray.toList.reverse)
    }
  }

  val password = Password("abcdefgh")
  println(LazyList.iterate(password)(_.next).find(_.isValid))
}
