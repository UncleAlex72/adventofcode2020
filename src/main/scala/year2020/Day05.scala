package year2020

import common.ResourceLoader

object Day05 extends App with ResourceLoader {

  sealed trait Bifurcation
  object Upper extends Bifurcation
  object Lower extends Bifurcation

  def bifurcate(bifurcations: Seq[Bifurcation]): Int = {
    case class Range(
        bottom: Int = 0,
        top: Int = (1 << bifurcations.length) - 1
    )
    val finalRange = bifurcations.foldLeft(Range()) {
      (previousRange, bifurcation) =>
        val lower = previousRange.bottom
        val upper = previousRange.top + 1
        val midPoint = lower + (upper - lower) / 2
        bifurcation match {
          case Upper => previousRange.copy(bottom = midPoint)
          case Lower => previousRange.copy(top = midPoint - 1)
        }
    }
    finalRange.top
  }

  def bifurcateString(
      parser: PartialFunction[Char, Bifurcation]
  )(str: String): Int = {
    bifurcate(str.toCharArray.map(parser))
  }

  val bifurcateFrontBack: String => Int = bifurcateString {
    case 'F' => Lower
    case 'B' => Upper
  }

  val bifurcateLeftRight: String => Int = bifurcateString {
    case 'L' => Lower
    case 'R' => Upper
  }

  def seatId(str: String): Int = {
    val (rowStr, columnStr) = str.splitAt(7)
    val row = bifurcateFrontBack(rowStr)
    val column = bifurcateLeftRight(columnStr)
    row * 8 + column
  }

  val rows = load("day05-input.txt")
  val seatIds = rows.map(seatId)

  val maxSeatId = seatIds.max
  println(seatIds.max)

  val mySeatId = seatIds
    .find { higherSeatId =>
      val seatId = higherSeatId - 1
      val lowerSeatId = higherSeatId - 2
      seatIds.contains(lowerSeatId) && !seatIds.contains(seatId)
    }
    .map(_ - 1)

  println(mySeatId)
}
