package year2020

import common.ResourceLoader

import scala.util.Try

object Day02 extends App with ResourceLoader {

  //8-9 n: nnnnnnnnn

  case class Row(min: Int, max: Int, letter: Char, password: String) {
    val partOneValid: Boolean = {
      val letterCount = password.count(_ == letter)
      letterCount >= min && letterCount <= max
    }
    val partTwoValid: Boolean = {
      def charAt(n: Int): Int = {
        Try(password.charAt(n - 1)).toOption.count(_ == letter)
      }

      (charAt(min) + charAt(max)) == 1
    }
  }

  object Row {
    def fromString(str: String): Option[Row] = {
      val regex = """([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)""".r
      str match {
        case regex(min, max, letter, password) =>
          Some(
            Row(
              Integer.parseInt(min),
              Integer.parseInt(max),
              letter.charAt(0),
              password
            )
          )
        case _ => None
      }
    }
  }

  val rows =
    load("day02-input.txt").flatMap(str => Row.fromString(str))
  println(rows.count(_.partOneValid))
  println(rows.count(_.partTwoValid))
}
