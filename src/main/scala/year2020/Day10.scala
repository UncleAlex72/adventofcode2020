package year2020

import cats.Eval
import common.ResourceLoader

import scala.collection.{SortedSet, mutable}

object Day10 extends App with ResourceLoader {

  val testData = """28
                   |33
                   |18
                   |42
                   |31
                   |14
                   |46
                   |20
                   |48
                   |47
                   |24
                   |23
                   |49
                   |45
                   |19
                   |38
                   |39
                   |11
                   |1
                   |32
                   |25
                   |35
                   |8
                   |17
                   |7
                   |9
                   |4
                   |2
                   |34
                   |10
                   |3""".rows

  val orderedData = load("day10-input.txt").map(_.toInt).sorted
  val allData = 0 +: orderedData :+ (orderedData.max + 3)

  val differences = allData.sliding(2, 1).map(jj => jj(1) - jj.head).toSeq

  val singleDifferences = differences.count(_ == 1L)
  val threeDifferences = differences.count(_ == 3L)

  println(singleDifferences * threeDifferences) // 1914

  // I didn't work this one out! My solution was a brute force search which took a while.
  // But this solution basically counts back the number of solutions from each number
  // which is the same but intrinsically memoizes.

  def countCombinations: Long = {
    val prepared = allData.tail
    val initial: Map[Int, Long] = Map(0 -> 1L)
    val countsFromNumber: Map[Int, Long] = prepared.foldLeft(initial) {
      (acc, adapter) =>
        val count = {
          val range = adapter - 3 until adapter
          val previous = range.map(acc.getOrElse(_, 0L))
          previous.sum
        }
        acc + (adapter -> count)
    }
    countsFromNumber(prepared.last)
  }

  println(countCombinations) // 9256148959232

}
