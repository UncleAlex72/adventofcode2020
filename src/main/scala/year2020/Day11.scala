package year2020

import common.{Cell, ResourceLoader}

object Day11 extends App with ResourceLoader {

  val testData =
    """
      |L.LL.LL.LL
      |LLLLLLL.LL
      |L.L.L..L..
      |LLLL.LL.LL
      |L.LL.LL.LL
      |L.LLLLL.LL
      |..L.L.....
      |LLLLLLLLLL
      |L.LLLLLL.L
      |L.LLLLL.LL""".rows

  val data = load("day11-input.txt")

  val initialGrid: Map[Cell, Boolean] = {
    val cells = for {
      (line, row) <- data.zipWithIndex
      (ch, column) <- line.toCharArray.zipWithIndex if ch == 'L'
    } yield {
      Cell(column = column, row = row) -> false
    }
    cells.toMap
  }

  case class Seats(
      grid: Map[Cell, Boolean],
      occupiedThreshold: Int,
      adjacentOnly: Boolean
  ) {

    val size: Int = grid.keys.flatMap(cell => Seq(cell.column, cell.row)).max

    val deltas: Seq[Cell] = {
      for {
        dx <- -1 to 1
        dy <- -1 to 1 if !(dx == 0 && dy == 0)
      } yield {
        Cell(column = dx, row = dy)
      }
    }

    def occupiedSeats: Int = {
      grid.count(_._2)
    }

    def findSeat(cell: Cell)(delta: Cell): Boolean = {
      val squaresToSearch = if (adjacentOnly) 1 else size
      (1 to squaresToSearch)
        .to(LazyList)
        .map(d => cell + delta * d)
        .flatMap(c => grid.get(c))
        .headOption
        .getOrElse(false)
    }
    def next: Seats = {
      val newGrid = grid.foldLeft(grid) {
        case (nextGrid, (cell, occupied)) =>
          val occupiedSeats = deltas.map(findSeat(cell)).count(identity)
          if (occupied && occupiedSeats >= occupiedThreshold) {
            nextGrid + (cell -> false)
          } else if (!occupied && occupiedSeats == 0) {
            nextGrid + (cell -> true)
          } else {
            nextGrid
          }
      }
      copy(grid = newGrid)
    }

    def stabilise: LazyList[Seats] = {
      val seatsList = LazyList.iterate(this)(_.next)
      seatsList
        .sliding(2, 1)
        .takeWhile(ss => ss.head != ss.last)
        .map(_.last)
        .to(LazyList)
    }

    override def toString: String = {
      val rows = grid.keys.map(_.row).max
      val columns = grid.keys.map(_.row).max
      val lines: Seq[String] = (0 to rows).map { row =>
        val chars = (0 to columns).map { column =>
          grid.get(Cell(column = column, row = row)) match {
            case Some(true)  => '#'
            case Some(false) => 'L'
            case None        => '.'
          }
        }
        chars.mkString
      }
      lines.mkString("\n")
    }
  }

  val partOneSeats =
    Seats(initialGrid, occupiedThreshold = 4, adjacentOnly = true).stabilise
  println(partOneSeats.last.occupiedSeats) // 2361

  val partTwoSeats =
    Seats(initialGrid, occupiedThreshold = 5, adjacentOnly = false).stabilise
  println(partTwoSeats.last.occupiedSeats) // 2119

}
