package year2020

import cats.data.NonEmptyList
import common.{Cell, AocParserBuilder, ResourceLoader}
import enumeratum._

object Day12 extends App with ResourceLoader {

  sealed trait Direction extends EnumEntry {
    val token: Char
    val delta: Cell
    val rotate: Direction
  }

  object Direction extends Enum[Direction] {

    case object North extends Direction {
      override val token: Char = 'N'
      override val delta: Cell = Cell(0, 1)
      override val rotate: Direction = East
    }

    case object East extends Direction {
      override val token: Char = 'E'
      override val delta: Cell = Cell(1, 0)
      override val rotate: Direction = South
    }

    case object South extends Direction {
      override val token: Char = 'S'
      override val delta: Cell = Cell(0, -1)
      override val rotate: Direction = West
    }

    case object West extends Direction {
      override val token: Char = 'W'
      override val delta: Cell = Cell(-1, 0)
      override val rotate: Direction = North
    }

    override def values: IndexedSeq[Direction] = findValues
  }

  sealed trait Instruction

  case class MoveInDirection(direction: Direction, distance: Int)
      extends Instruction

  case class Forward(distance: Int) extends Instruction

  case class Rotate(angle: Int) extends Instruction

  object Rotate {

    def right(angle: Int): Rotate = Rotate(angle)

    def left(angle: Int): Rotate = Rotate(360 - angle)
  }

  object InstructionParserBuilder extends AocParserBuilder[Instruction] {

    def char(ch: Char): Parser[Char] =
      s"$ch" ^^ {
        _.charAt(0)
      }

    def number: Parser[Int] =
      """\d+""".r ^^ {
        _.toInt
      }

    def single(ch: Char, fn: Int => Instruction): Parser[Instruction] = {
      (char(ch) ~> number) ^^ fn
    }

    def directions: Seq[Parser[Instruction]] = {
      Direction.values.map { direction =>
        single(direction.token, MoveInDirection(direction, _))
      }
    }

    def rotateRight: Parser[Instruction] = {
      single('R', Rotate.right)
    }

    def rotateLeft: Parser[Instruction] = {
      single('L', Rotate.left)
    }

    def forward: Parser[Instruction] = {
      single('F', Forward)
    }

    override def build: Parser[Instruction] = {
      directions.foldLeft(rotateRight | rotateLeft | forward)(_ | _)
    }
  }

  val testData =
    """
      |F10
      |N3
      |F7
      |R90
      |F11""".rows

  val data = load("day12-input.txt")

  def manhattanDistance(start: Cell, finish: Cell): Int = {
    Math.abs(finish.row - start.row) + Math.abs(finish.column - start.column)
  }

  val instructionParser = InstructionParserBuilder()
  val instructions = data.flatMap(instructionParser.maybeParse)

  case class Ship(
      position: Cell,
      facing: Direction
  ) {

    def instruct(instruction: Instruction): Ship = {
      instruction match {
        case MoveInDirection(direction, distance) =>
          copy(position = position + direction.delta * distance)
        case Forward(distance) =>
          copy(position = position + facing.delta * distance)
        case Rotate(angle) =>
          val newDirection =
            (0 until (angle / 90)).foldLeft(facing)((direction, _) =>
              direction.rotate
            )
          copy(facing = newDirection)
      }
    }
  }
  val ship: Ship = instructions.foldLeft(Ship(Cell(0, 0), Direction.East))(
    _.instruct(_)
  )
  println(manhattanDistance(Cell(0, 0), ship.position)) // 2879

  case class ShipWithWaypoint(position: Cell, waypoint: Cell) {

    def instruct(instruction: Instruction): ShipWithWaypoint = {
      instruction match {
        case MoveInDirection(direction, distance) =>
          copy(waypoint = waypoint + direction.delta * distance)
        case Forward(distance) =>
          copy(position = position + waypoint * distance)
        case Rotate(angle) =>
          val newWaypoint = (0 until (angle / 90)).foldLeft(waypoint) {
            (cell, _) =>
              cell.copy(column = cell.row, row = -cell.column) //(y,-x)
          }
          copy(waypoint = newWaypoint)
      }
    }
  }

  val shipWithWaypoint: ShipWithWaypoint = instructions.foldLeft(
    ShipWithWaypoint(
      Cell(0, 0),
      Cell(0, 0) + Direction.East.delta * 10 + Direction.North.delta
    )
  )(
    _.instruct(_)
  )

  println(manhattanDistance(Cell(0, 0), shipWithWaypoint.position)) // 178986

}
