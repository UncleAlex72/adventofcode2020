package year2020

import common.Sequences._
import common.{AocParserBuilder, ResourceLoader}

import scala.annotation.tailrec
object Day13 extends App with ResourceLoader {

  object BusParserBuilder extends AocParserBuilder[Map[Int, Int]] {

    def number: Parser[Int] =
      """\d+""".r ^^ {
        _.toInt
      }

    def numberOrX: Parser[Option[Int]] = {
      (number ^^ { Some(_) }) | ("x" ^^ { _ =>
        None
      })
    }
    override def build: Parser[Map[Int, Int]] = {
      numberOrX ~ ("," ~> numberOrX).* ^^ {
        case first ~ next =>
          val busesByIndex = (first :: next).zipWithIndex.flatMap {
            case (maybeId, idx) => maybeId.map(id => idx -> id)
          }
          busesByIndex.toMap
      }
    }
  }

  val testData =
    """
      |939
      |7,13,x,x,59,x,31,19""".rows

  val data = load("day13-input.txt")

  val startTime = data.head.toInt

  val busParser = BusParserBuilder()
  val buses = busParser.valueOf(data(1))

  val nextBusTimeWaits =
    buses.values.map(id => id -> (id - startTime % id)).toMap
  val partOneResult: Int = {
    val (id, wait) = nextBusTimeWaits.minBy(_._2)
    id * wait
  }
  println(partOneResult) // 296

  def chineseRemainder(n: List[Long], a: List[Long]): Long = {

    require(n.size == a.size)
    val prod = n.product

    @tailrec def iter(n: List[Long], a: List[Long], sm: Long): Long = {
      def mulInv(a: Long, b: Long): Long = {
        @tailrec def loop(a: Long, b: Long, x0: Long, x1: Long): Long = {
          if (a > 1) loop(b, a % b, x1 - (a / b) * x0, x0) else x1
        }

        if (b == 1) 1L
        else {
          val x1 = loop(a, b, 0, 1)
          if (x1 < 0) x1 + b else x1
        }
      }

      if (n.nonEmpty) {
        val p = prod / n.head

        iter(n.tail, a.tail, sm + a.head * mulInv(p, n.head) * p)
      } else sm
    }

    iter(n, a, 0) % prod
  }

  val partTwoResult = {
    val (remainders, primes) =
      buses.foldLeft((Seq.empty[Long], Seq.empty[Long])) {
        case ((remainders, primes), (idx, id)) =>
          (remainders :+ (id - idx), primes :+ id)
      }
    chineseRemainder(primes.toList, remainders.toList)
  }

  println(partTwoResult) // 535296695251210

}
