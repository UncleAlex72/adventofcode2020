package year2020

import common.{AocParserBuilder, ResourceLoader}

import scala.annotation.tailrec

object Day08 extends App with ResourceLoader {

  sealed trait Action
  case object Nop extends Action
  case object Acc extends Action
  case object Jmp extends Action

  case class Instruction(action: Action, value: Int)

  object InstructionParserBuilder extends AocParserBuilder[Instruction] {

    private def action: Parser[Action] = {
      "nop" ^^ { _ => Nop } | "acc" ^^ { _ => Acc } | "jmp" ^^ { _ => Jmp }
    }

    private def value: Parser[Int] = """[+-]?\d+""".r ^^ { _.toInt }

    override def build: Parser[Instruction] = {
      (action ~ value) ^^ { case action ~ value => Instruction(action, value) }
    }
  }

  case class Automata(
      instructions: Map[Int, Instruction],
      visitedIndicies: Set[Int] = Set.empty,
      idx: Int = 0,
      acc: Int = 0,
      status: Boolean = false
  ) {

    @tailrec final def run: Automata = {
      val maybeInstruction: Option[Instruction] =
        instructions.get(idx).filterNot(_ => visitedIndicies.contains(idx))
      maybeInstruction match {
        case Some(instruction) =>
          val visited = copy(visitedIndicies = visitedIndicies + idx)
          val nextAutomata = instruction.action match {
            case Nop => visited.copy(idx = idx + 1)
            case Acc =>
              visited.copy(idx = idx + 1, acc = acc + instruction.value)
            case Jmp => visited.copy(idx = idx + instruction.value)
          }
          nextAutomata.run
        case None => copy(status = idx == instructions.size)
      }
    }
  }

  val testData =
    """
      |nop +0
      |acc +1
      |jmp +4
      |acc +3
      |jmp -3
      |acc -99
      |acc +1
      |jmp -4
      |acc +6""".rows

  val data = load("day08-input.txt")

  val instructionParser = InstructionParserBuilder()
  val instructions =
    data
      .flatMap(instructionParser.maybeParse)
      .zipWithIndex
      .map(_.swap)
      .toMap

  val automata = Automata(instructions)
  println(automata.run.acc) // 1797

  val alteredInstructions: Seq[Map[Int, Instruction]] = {
    val empty: Seq[Map[Int, Instruction]] = Seq.empty
    instructions.keys.foldLeft(empty) { (maps, idx) =>
      instructions.get(idx) match {
        case Some(Instruction(Nop, value)) =>
          maps :+ instructions + (idx -> Instruction(Jmp, value))
        case Some(Instruction(Jmp, value)) =>
          maps :+ instructions + (idx -> Instruction(Nop, value))
        case _ => maps
      }
    }
  }

  val maybeResult =
    alteredInstructions.to(LazyList).map(Automata(_).run).find(_.status)
  println(maybeResult.map(_.acc)) // Some(1036)
}
