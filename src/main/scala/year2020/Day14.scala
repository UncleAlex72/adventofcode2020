package year2020

import common.{Cell, AocParserBuilder, ResourceLoader}

object Day14 extends App with ResourceLoader {

  sealed trait Instruction

  case class Memory(location: Long, value: Long) extends Instruction

  case class Mask(bitmask: String) extends Instruction {

    private def convertToBinary(number: Long): Seq[Char] = {
      val raw = number.toBinaryString
      raw.prependedAll(LazyList.continually('0').take(36 - raw.length))
    }

    private def convertToLong(binary: Seq[Char]): Long = {
      java.lang.Long.parseLong(binary.mkString, 2)
    }

    def maskValue(number: Long): Long = {
      val maskedNumber: Seq[Char] =
        convertToBinary(number).zip(bitmask).map {
          case (digit, mask) =>
            if (mask == 'X') {
              digit
            } else {
              mask
            }
        }
      convertToLong(maskedNumber)
    }

    def maskAddress(address: Long): Seq[Long] = {
      val maskedCandidates: Seq[Seq[Char]] =
        convertToBinary(address).zip(bitmask) map {
          case (digit, '0') => Seq(digit)
          case (_, '1')     => Seq('1')
          case (_, _)       => Seq('0', '1')
        }
      val bifurcated: Seq[Seq[Char]] = {
        val empty: Seq[Seq[Char]] = Seq(Seq.empty)
        maskedCandidates.foldLeft(empty) {
          case (acc, digits) =>
            for {
              candidates <- acc
              digit <- digits
            } yield {
              candidates :+ digit
            }
        }
      }
      bifurcated.map(convertToLong)
    }
  }

  object InstructionParserBuilder extends AocParserBuilder[Instruction] {

    def bitmap: Parser[String] = "[01X]{36}".r ^^ { _.toString }

    def number: Parser[Long] =
      """\d+""".r ^^ {
        _.toLong
      }

    def mask: Parser[Instruction] = "mask = " ~> bitmap ^^ { Mask }

    def mem: Parser[Instruction] =
      ("mem[" ~> number) ~ ("] = " ~> number) ^^ {
        case location ~ value => Memory(location.toInt, value)
      }
    override def build: Parser[Instruction] = {
      mask | mem
    }
  }

  val testData =
    """
      |mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
      |mem[8] = 11
      |mem[7] = 101
      |mem[8] = 0""".rows

  val data = load("day14-input.txt")

  case class State(
      mask: Option[Mask] = None,
      memory: Map[Long, Long] = Map.empty
  ) {

    def partOne(instruction: Instruction): State = {
      instruction match {
        case mask: Mask => copy(mask = Some(mask))
        case Memory(location, value) =>
          mask
            .map { mask =>
              val maskedValue = mask.maskValue(value)
              copy(memory = memory + (location -> maskedValue))
            }
            .getOrElse(this)
      }
    }

    def partTwo(instruction: Instruction): State = {
      instruction match {
        case mask: Mask => copy(mask = Some(mask))
        case Memory(location, value) =>
          val locations =
            mask.toSeq.flatMap(_.maskAddress(location)).map(_ -> value)
          copy(memory = memory ++ locations)
      }
    }
    def sum: Long = {
      memory.values.sum
    }
  }

  val instructionParser = InstructionParserBuilder()
  val instructions = data.flatMap(instructionParser.maybeParse)

  val partOneState = instructions
    .foldLeft(State())(_.partOne(_))

  println(partOneState.sum)

  val partTwoState = instructions
    .foldLeft(State())(_.partTwo(_))

  println(partTwoState.sum)

}
