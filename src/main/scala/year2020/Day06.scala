package year2020

import common.ResourceLoader
import common.Sequences._

object Day06 extends App with ResourceLoader {

  val groupedRows: Seq[Seq[String]] =
    load("day06-input.txt").splitWhen(_.isEmpty)

  val answers: Seq[Set[Char]] = groupedRows.map { rows =>
    rows.flatMap(_.toCharArray).toSet
  }

  println(answers.map(_.size).sum)

  val commonAnswerCounts = groupedRows.map { rows =>
    val answerSets: Seq[Set[Char]] = rows.map(_.toCharArray.toSet)
    answerSets.reduce(_.intersect(_)).size
  }

  println(commonAnswerCounts.sum)
}
