package year2020

import common.ResourceLoader
import common.Sequences._

object Day03 extends App with ResourceLoader {

  val rows = load("day03-input.txt").map(_.trim).filterNot(_.isEmpty)

  def countTrees(right: Int, down: Int) = {
    val points = rows.sliding(down, down).map(_.head).zipWithIndex.map {
      case (row, idx) =>
        val posn = (idx * right) % row.length
        row.charAt(posn)
    }
    points.count(_ == '#')
  }

  //Right 1, down 1.
  //Right 3, down 1. (This is the slope you already checked.)
  //Right 5, down 1.
  //Right 7, down 1.
  //Right 1, down 2.
  val slopes: Seq[(Int, Int)] = Seq((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))
  val trees = slopes.map(dir => countTrees(dir._1, dir._2)).multiply
  println(trees)
}
