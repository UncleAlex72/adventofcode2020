package year2020

import common.{AocParserBuilder, ResourceLoader}

import scala.annotation.tailrec

object Day09 extends App with ResourceLoader {

  case class Encryption(numbers: Seq[Long], preambleLength: Int) {

    def findNonSumNumber: Option[Long] = {
      val indicies = (preambleLength to numbers.length).to(LazyList)
      val maybeIndex = indicies.find { idx =>
        val previousNumbers = numbers.slice(idx - preambleLength, idx)
        val previousSums = previousNumbers.combinations(2).map(_.sum)
        !previousSums.contains(numbers(idx))
      }
      maybeIndex.map(idx => numbers(idx))
    }
    def findSequence(nonSumNumber: Long): Option[Seq[Long]] = {
      val subsequences = for {
        ys <- numbers.inits.to(LazyList)
        zs <- ys.tails.to(LazyList) if zs.length >= 2
      } yield {
        zs
      }
      subsequences.find(ss => ss.sum == nonSumNumber)
    }

    def sumMaxAndMin(sequence: Seq[Long]): Long = {
      sequence.max + sequence.min
    }
  }

  object Encryption {
    def apply(preambleLength: Int, numbers: Seq[String]): Encryption = {
      Encryption(numbers.map(_.toLong), preambleLength)
    }

  }
  val data = load("day09-input.txt")
  val encryption = Encryption(25, data)

  val maybeResult = for {
    nonSumNumber <- encryption.findNonSumNumber
    sequence <- encryption.findSequence(nonSumNumber)
  } yield {
    (nonSumNumber, encryption.sumMaxAndMin(sequence))
  }

  println(maybeResult) // Some((552655238,70672245))
}
