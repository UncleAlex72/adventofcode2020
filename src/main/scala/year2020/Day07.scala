package year2020

import common.{AocParserBuilder, ResourceLoader}

object Day07 extends App with ResourceLoader {

  case class Bag(colour: String, contains: Map[String, Int] = Map.empty)

  object BagParserBuilder extends AocParserBuilder[Bag] {

    private def word: Parser[String] = """[a-z]+""".r ^^ { _.toString }
    private def number: Parser[Int] = """[0-9]+""".r ^^ { _.toInt }
    private def bags: Parser[String] = literal("bags") | literal("bag")
    private def comma: Parser[String] = literal(",")
    private def fullStop: Parser[String] = literal(".")
    private def contains: Parser[String] = literal("contain")
    private def colour: Parser[String] =
      (word ~ word).map { case x ~ y => s"$x $y" }

    private def numberOfBags: Parser[Map[String, Int]] = {
      (number ~ colour ~ bags).map {
        case number ~ colour ~ _ =>
          Map(colour -> number)
      }
    }

    private def listOfNumberOfBags: Parser[Map[String, Int]] = {
      (numberOfBags ~ (comma ~> numberOfBags).*).map {
        case m ~ ms =>
          ms.foldLeft(m)(_ ++ _)
      }
    }

    private def noBags: Parser[Map[String, Int]] = {
      literal("no other bags").map(_ => Map.empty)
    }

    private def contents: Parser[Map[String, Int]] = listOfNumberOfBags | noBags

    override def build: Parser[Bag] = {
      ((colour <~ bags <~ contains) ~ contents <~ fullStop).map {
        case colour ~ contains =>
          Bag(colour, contains)
      }
    }

  }

  val data = load("day07-input.txt")
  val bagParser = BagParserBuilder()
  val bags = data.flatMap(bagParser.maybeParse)

  def canContain(colour: String): Set[Bag] = {
    val directContains: Set[Bag] =
      bags.filter(_.contains.keys.toSeq.contains(colour)).toSet
    directContains.foldLeft(directContains) { (bags, bag) =>
      bags ++ canContain(bag.colour)
    }
  }

  private val canContainBags: Set[Bag] = canContain("shiny gold")
  println(canContainBags.size) // 213

  def filledUp(colour: String): Int = {
    bags.find(_.colour == colour) match {
      case Some(bag) =>
        val contents = bag.contains.map {
          case (colour, number) =>
            filledUp(colour) * number
        }
        contents.sum + 1
      case None => 0
    }
  }

  private val filledUpBag: Int = filledUp("shiny gold") - 1
  println(filledUpBag) // 213
}
