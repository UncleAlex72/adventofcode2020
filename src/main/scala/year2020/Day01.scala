package year2020

import common.ResourceLoader
import common.Sequences._

import scala.util.Try

object Day01 extends App with ResourceLoader {

  val data = load("day01-input.txt")
    .flatMap(str => Try(Integer.parseInt(str)).toOption)
    .to(LazyList)

  // Some(876459)
  //Some(116168640)
  def result(i: Int): Option[Long] = {
    data
      .combinations(i)
      .to(LazyList)
      .find(nums => nums.sum == 2020)
      .map(nums => nums.multiply)
  }

  println(result(2))
  println(result(3))

}
