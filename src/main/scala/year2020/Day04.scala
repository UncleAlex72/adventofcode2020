package year2020

import common.ResourceLoader

import scala.util.Try
import common.Sequences._

object Day04 extends App with ResourceLoader {

  val groupedRows: Seq[Seq[String]] =
    load("day04-input.txt").map(_.trim).splitWhen(_.isEmpty)

  val passportData: Seq[Map[String, String]] = groupedRows.map { lines =>
    val empty: Map[String, String] = Map.empty
    lines.flatMap(line => line.split("""\s+""")).foldLeft(empty) {
      case (acc, kv) =>
        val sepIdx = kv.indexOf(':')
        if (sepIdx < 0) {
          acc
        } else {
          val k = kv.substring(0, sepIdx)
          val v = kv.substring(sepIdx + 1, kv.length)
          acc + (k.trim -> v.trim)
        }
    }
  }

  def validateField(
      key: String
  )(validator: String => Boolean)(passport: Map[String, String]): Boolean = {
    passport.get(key).exists(validator)
  }

  val validateBetween: String => (Int, Int) => Map[String, String] => Boolean =
    key =>
      (min, max) =>
        validateField(key) { value =>
          {
            Try(Integer.parseInt(value)).toOption.exists(year =>
              year >= min && year <= max
            )
          }
        }

  val validateRegex: String => String => Map[String, String] => Boolean = key =>
    regex => validateField(key)(regex.r.matches)

  val validateByr: Map[String, String] => Boolean =
    validateBetween("byr")(1920, 2002)
  val validateIyr: Map[String, String] => Boolean =
    validateBetween("iyr")(2010, 2020)
  val validateEyr: Map[String, String] => Boolean =
    validateBetween("eyr")(2020, 2030)
  val validateHgt: Map[String, String] => Boolean = {
    validateField("hgt") { value =>
      val regex = """([0-9]+)(cm|in)""".r
      value match {
        case regex(sizeString, unit) =>
          val size = Integer.parseInt(sizeString)
          unit match {
            case "cm" => size >= 150 && size <= 193
            case "in" => size >= 59 && size <= 76
            case _    => false
          }
        case _ => false
      }
    }
  }
  val validateHcl = validateRegex("hcl")("#[0-9a-f]{6}")
  val validateEcl = validateRegex("ecl")("amb|blu|brn|gry|grn|hzl|oth")
  val validatePid = validateRegex("pid")("[0-9]{9}")

  val validators = LazyList(
    validateByr,
    validateIyr,
    validateEyr,
    validateHgt,
    validateHcl,
    validateEcl,
    validatePid
  )

  val validPassports = passportData.count { passport =>
    validators.forall(validator => validator(passport))
  }
  println(validPassports)
}
