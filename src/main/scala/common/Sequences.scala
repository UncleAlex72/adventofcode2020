package common

trait Sequences {

  implicit class SequenceMultiplication(ints: Iterable[Int]) {
    def multiply: Long = ints.foldLeft(1L)(_ * _)
  }

  implicit class SequenceLogic(bools: Iterable[Boolean]) {
    def and: Boolean = bools.forall(b => b)
  }

  implicit class SequenceExtensions[A](as: Iterable[A]) {
    def split(shards: Int): Map[Int, Seq[A]] = {
      val empty: Map[Int, Seq[A]] = Map.empty
      as.zipWithIndex.foldLeft(empty) {
        case (map, (a, idx)) =>
          val shard = idx % shards
          val sequenceForShard: Seq[A] = map.getOrElse(shard, Seq.empty)
          map + (shard -> (sequenceForShard :+ a))
      }
    }

    def splitWhen(predicate: A => Boolean): Seq[Seq[A]] = {
      case class State(
          previousGroups: Seq[Seq[A]] = Seq.empty,
          currentGroup: Seq[A] = Seq.empty
      ) {
        def groups: Seq[Seq[A]] =
          (previousGroups :+ currentGroup).filterNot(_.isEmpty)
        def next(a: A): State = {
          if (predicate(a)) {
            State(previousGroups = groups, currentGroup = Seq.empty)
          } else {
            copy(currentGroup = currentGroup :+ a)
          }
        }
      }
      val finalState = as.foldLeft(State())(_.next(_))
      finalState.groups
    }
  }
}

object Sequences extends Sequences
