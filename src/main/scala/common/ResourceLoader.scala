package common

import scala.io.Source

trait ResourceLoader {

  def load(resourceName: String): Seq[String] = {
    val resourceUrl = this.getClass.getResource(resourceName)
    val source = Source.fromFile(resourceUrl.toURI, "UTF-8")
    val lines = source.getLines().toSeq
    source.close
    lines
  }

  def loadSingle(resourceName: String): String = {
    load(resourceName).mkString("\n")
  }

  implicit class TestDataImplicits(data: String) {
    def rows: Seq[String] = data.stripMargin.split("\n").filterNot(_.isEmpty)
  }
}
