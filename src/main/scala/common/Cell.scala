package common

case class Cell(column: Int, row: Int) {

  def +(cell: Cell): Cell = {
    Cell(column = cell.column + column, row = cell.row + row)
  }

  def *(value: Int): Cell = {
    Cell(column = column * value, row = row * value)
  }
}

object Cell {

  implicit val cellOrdering: Ordering[Cell] =
    Ordering.by(cell => (cell.row, cell.column))
}
