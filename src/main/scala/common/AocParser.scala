package common

import scala.util.parsing.combinator.RegexParsers

trait AocParser[A] {

  def parse(str: String): Either[String, A]

  def maybeParse(str: String): Option[A] = parse(str).toOption

  def valueOf(str: String): A = maybeParse(str).get
}

trait AocParserBuilder[A] extends RegexParsers {

  def build: Parser[A]

  def apply(): AocParser[A] =
    (str: String) => {
      AocParserBuilder.this.parse(build, str) match {
        case Success(a, _)   => Right(a)
        case Error(msg, _)   => Left(msg)
        case Failure(msg, _) => Left(msg)
      }
    }
}
